$(function () { // Create the chart
$( ".period_stats_info_chart.area" ).each(function() {
	var data = $.map($(this).data("series").split(','), parseFloat);
	Highcharts.chart( $( this ).attr("id"), {
		chart: {
			type: 'area',
			backgroundColor: $(this).hasClass("even")?'#ff6666':"#002741",
			margin:0
		},
		title: {
			text: ''
		},
		xAxis: {
			visible: false,
		},
		yAxis: {
			visible: false,
		},
		plotOptions: {
			area: {
				fillColor:$(this).hasClass("even")?'#d85656':"#59798a",
				lineColor: $(this).hasClass("even")?'#d85656':"#59798a",
				pointStart: 1940,
				marker: {
					enabled: false,
					symbol: 'circle',
					radius: 1,
					states: {
						hover: {
							enabled: false
						}
					}
				}
			}
		},
		legend: {
			enabled: false
		},
		series: [{
			data: data
		}]
	});
});

Highcharts.chart('container_pie_chart', {
    chart: {
        type: 'pie'
    },
    title: {
        text: 'Incidents by infrigment type'
    },
    plotOptions: {
        series: {
            dataLabels: {
                enabled: true,
                format: '{point.name}: {point.y:.1f}%'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
    },

    "series": [
        {
            "name": "Browsers",
            "colorByPoint": true,
            "data": [
                {
                    "name": "Trademark",
                    "y": 10.3,
                    "drilldown": "Trademark"
                },
                {
                    "name": "Copyright",
                    "y": 20.5,
                    "drilldown": "Copyright"
                },
                {
                    "name": "Gray Maarket",
                    "y": 23.2,
                    "drilldown": "Gray Maarket"
                },
                {
                    "name": "Counterfeit",
                    "y": 46,
                    "drilldown": "Safari"
                }
             ]
        }
    ]
});
});