<?php 
	require_once("classes\PeriodStat.php");
	require_once("classes\Product.php");
	require_once("classes\Brand.php");
	require_once("classes\Entity.php");
	$periodStats = PeriodStat::getDashboardStats();
	$products    = Product::getDashboarProducts();
	$brands		 = Brand::getDashboardBrands();
	$entities	 = Entity::getDashboardEntities();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="amassillo">
    <link rel="icon" type="image/png" sizes="16x16" href="../plugins/images/favicon.png">
    <title>GoManzanas exam</title>
    <!-- Bootstrap Core CSS -->
    <link href="bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="css/colors/default.css" id="theme" rel="stylesheet">
	
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="https://code.highcharts.com/highcharts.js"></script>
	<script src="https://code.highcharts.com/maps/modules/map.js"></script>
	<script src="https://code.highcharts.com/maps/modules/data.js"></script>
	<script src="https://code.highcharts.com/modules/drilldown.js"></script>
	<script src="https://code.highcharts.com/mapdata/custom/world.js"></script>

	<script src="js/map.js"></script>
	<script src="js/pie_chart.js?random=<?php echo uniqid(); ?>"></script>
</head>

<body class="fix-header">
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li>
                        <form role="search" class="app-search hidden-sm hidden-xs m-r-10">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li>
                    <li>
                        <a class="profile-pic" href="#"> <img src="../plugins/images/users/varun.jpg" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">Steave</b></a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Brand protection q1 2018</h4>
					</div>
                </div>
                <!-- /.row -->
                <!-- ============================================================== -->
                <!-- Different data widgets -->
                <!-- ============================================================== -->
                <!-- .row -->
                <div class="row">
					<!-- max 3, -->
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info">
                            <h3 class="box-title">Output</h3>
							<!--title -->
							<ul class="list-inline two-part">
                                <li>
                                    # Incidents
                                </li>
                                <li class="text-right"># Test Buys</li>
                            </ul>
							<!-- end of title -->
                            <ul class="list-inline two-part">
                                <li>
                                  <span>82</span>
                                </li>
                                <li class="text-right">
									<span>82</span>
								</li>
                            </ul>
                        </div>
                    </div>
					 <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info">
                            <h3 class="box-title">Recovery</h3>
							<!--title -->
							<ul class="list-inline two-part">
                                <li>
                                    Open
                                </li>
                                <li class="text-right">Closed</li>
                            </ul>
							<!-- end of title -->
                            <ul class="list-inline two-part">
                                <li>
                                  <span>$30,000</span>
                                </li>
                                <li class="text-right">
									<span>$40,000</span>
								</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                        <div class="white-box analytics-info">
                            <h3 class="box-title">Prevention</h3>
							<!--title -->
							<ul class="list-inline two-part">
                                <li>
                                    Open
                                </li>
                                <li class="text-right">Closed</li>
                            </ul>
							<!-- end of title -->
                            <ul class="list-inline two-part">
                                <li>
                                   <span> 5</span>
                                </li>
                                <li class="text-right">
									<span>911</span>
								</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--/.row -->
				<!-- ============================================================== -->
                <!-- area charts>
                <!-- ============================================================== -->
				<div class="row">
					<?php foreach ($periodStats as $index => $periodStat) { ?>
					 <div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info no-padding">
								<ul class="list-inline">
									<div class="stats_info">
										<table class="details">
											<tr>
												<th class="title" colspan="4"><?php echo $periodStat->getName()?></th>
											</tr>
											<tr>
												<th>QoQ change</th>
												<th>YoY change</th>
												<th># Open</th>
												<th># Closed</th>
											</tr>
											<tr>
												<?php ?>
												<td><i class="fa fa-caret-<?php echo $periodStat->getQoQChange() >0 ?"up":"down"?>"></i><?php echo abs($periodStat->getQoQChange())?></td> <!-- NEEDTO FORMAT-->
												<td><i class="fa fa-caret-<?php echo $periodStat->getYoYChange() >0 ?"up":"down"?>"></i><?php echo abs($periodStat->getYoYChange())?></td>
												<td class="centered"><?php echo $periodStat->getOpen()?></td>
												<td class="centered"><?php echo $periodStat->getClosed()?></td>
											</tr>
										</table>
									</div>
									<div id="achart<?php echo $index?>" class="period_stats_info_chart area <?php echo $index %2 ==0 ? "even":"odd"?>" data-series="<?php echo $periodStat->getStringSerieValues();?>"></div>
								</ul>
							</div>
						</div>
						<?php }?>
						<div class="col-lg-4 col-sm-6 col-xs-12">
							<div class="white-box analytics-info no-padding">
								<div id="container_pie_chart" class="period_stats_info_chart"></div>
							</div>	
						</div>	
				</div>	
				<!-- ============================================================== -->
                <!-- tables -->
                <!-- ============================================================== -->
                <div class="row">
                    <!-- .col -->
                    <div class="col-lg-8 col-md-12 col-sm-12">
                        <div class="white-box">
                           <div id="container" ></div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6 col-sm-12">
                        <div class="white-box">
							<h3 class="box-title">Products</h3>
							<div class="table-responsive">
                                <table class="table" >
                                    <thead>
                                        <tr>
                                            <th>Product name</th>
                                            <th>Brand</th>
                                            <th># incidents</th>
											<th>Thrend</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php foreach ($products as $product) { ?>
                                        <tr>
                                            <td class="txt-oflo"><?php echo $product->getName()?></td>
                                            <td class="txt-oflo"><?php echo $product->getBrand()?></td>
                                            <td class="txt-right"><?php echo $product->getIncidents()?></td>
											<td data-sparkline="<?php echo $product->getStringTrendValues()?>"></td>
                                        </tr>
										<?php }?>
									</tbody>
								</table>
							</div>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
            </div>
                <!-- ============================================================== -->
                <!-- table -->
                <!-- ============================================================== -->
                <div class="row">
				<div class="col-lg-4 col-md-1 col-sm-1">
                        <div class="white-box">
                            <h3 class="box-title">Brands</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Brand</th>
                                            <th>Thrend</th>
                                            <th># incidents</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php foreach ($brands as $brand) { ?>
                                        <tr>
                                            <td class="txt-oflo"><?php echo $brand->getBrand()?></td>
                                            <td  data-sparkline="<?php echo $brand->getStringTrendValues()?>"></td>
                                            <td class="txt-right"><?php echo $brand->getIncidents()?></td>
                                        </tr>
										<?php }?>
                                    </tbody>
                                </table>
                            </div>
							<div class="row">
								<a href="#">Show more</a>
							</div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-2 col-sm-2">
                        <div class="white-box">
                            <h3 class="box-title">Entities</h3>
                            <div class="table-responsive">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Entity</th>
                                            <th>Customs</th>
                                            <th>Online</th>
                                            <th>Other</th>
                                            <th>Marketplaces</th>
											<th>Products</th>
                                        </tr>
                                    </thead>
                                    <tbody>
										<?php foreach ($entities as $entity) { ?>
                                        <tr>
                                            <td class="txt-oflo"><?php echo $entity->getName() ?></td>
                                            <td><?php echo $entity->getCustoms() ?></td>
                                            <td><?php echo $entity->getOnline() ?></td>
                                            <td><?php echo $entity->getOther() ?></td>
                                            <td><?php echo $entity->getMarketplaces() ?></td>
											<td><?php echo $entity->getProducts() ?></td>
                                        </tr>
										<?php }?>
                                    </tbody>
                                </table>
                            </div>
							<div class="row">
								<a href="#">Show more</a>
							</div>
                        </div>
                    </div>
                </div>
         </div>
        <!-- ============================================================== -->
        <!-- End Page Content -->
        <!-- ============================================================== -->
    </div>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
	<script src="js/trend_chart.js"></script>
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    <script src="../plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="bootstrap/dist/js/bootstrap.min.js"></script>
    
</body>

</html>
