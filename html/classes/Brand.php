<?php 

	/**
	author: amassillo
	**/
	class Brand { 
		private $name; 
		private $incidents; 
		private $trendValues;
    
		function __construct($pBrand,$pIncidents,$pTrendValues) {
			$this->brand = $pBrand;
			$this->incidents = $pIncidents;
			$this->trendValues 	  = $pTrendValues;
		}

		function getBrand() { 
			return $this->brand; 
		} 
		
		function getIncidents() { 
			return $this->incidents; 
		} 
				
		function getTrendValues() { 
			return $this->trendValues; 
		} 
		
		function getStringTrendValues(){
			return implode(', ',$this->trendValues);
		}
		static function getDashboardBrands(){
			
			return array(new Brand("Samsung",42,[4,5,7,6,8,8,8,9,10,20,20,18,22,17]),
						 new Brand("HP",12,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Apple",39,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Fuji",23,[4,5,7,60,20,20,18,22,17]),
						 new Brand("LG",24,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Cannon",21,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Sony",36,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Nikon",13,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Phantom",9,[4,5,7,60,20,20,18,22,17]),
						 new Brand("Boosted",6,[4,5,7,60,20,20,18,22,17]));
		}
} 


?>