<?php 

	/**
	author: amassillo
	**/
	class Product { 
		private $name; 
		private $brand; 
		private $incidents; 
		private $trendValues;
    
		function __construct($pName, $pBrand,$pIncidents,$pTrendValues) {
			$this->name = $pName;
			$this->brand = $pBrand;
			$this->incidents = $pIncidents;
			$this->trendValues 	  = $pTrendValues;
		}
		function getName() { 
			return $this->name; 
		} 
		
		function getBrand() { 
			return $this->brand; 
		} 
		
		function getIncidents() { 
			return $this->incidents; 
		} 
				
		function getTrendValues() { 
			return $this->trendValues; 
		} 
		
		function getStringTrendValues(){
			return implode(', ',$this->trendValues);
		}
		static function getDashboarProducts(){
			
			return array(new Product("Resident Evil","Samsung",5,[4,5,7,6,8,8,8,9]),
						 new Product("Kane & Linch","HP",5,[4,5,7,6,8,8,8,9,22,17]),
						 new Product("InFaous","Apple",5,[4,5,7,6,20,18,22,17]),
						 new Product("Final Fantasy X","Fuji",5,[4,5,720,20,18,22,17]),
						 new Product("Prototype","LG",5,[4,5,7,6,8,8,8,9,10,20,20,18,22,17]),
						 new Product("Kane & Linch","Canon",5,[4,5,7,6,0,20,20,18,22,17]),
						 new Product("Dante's inferno","HP",5,[4,5,7,6,8,8,17])
						 );
		}
} 


?>