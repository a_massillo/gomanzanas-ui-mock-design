<?php 

	/**
	author: amassillo
	**/
	class Entity { 
		private $name; 
		private $customs; 
		private $online; 
		private $marketplaces;
		private $products;
    
		function __construct($pName, $pCustoms,$pOnline,$pOther,$pMarketplaces, $pProducts) {
			$this->name 	= $pName;
			$this->customs 	= $pCustoms;
			$this->online  	= $pOnline;
			$this->other 	  = $pOther;
			$this->marketplaces   = $pMarketplaces;
			$this->products 	  = $pProducts;
		}
		function getName() { 
			return $this->name; 
		} 
		
		function getCustoms() { 
			return $this->customs; 
		} 
		
		function getOnline() { 
			return $this->online; 
		} 
				
		function getOther() { 
			return $this->other; 
		} 
		
		function getMarketplaces(){
			return $this->marketplaces;
		}
		function getProducts(){
			return $this->products;
		}
		static function getDashboardEntities(){
			
			return array(new Entity("Audi",2,2,2,2,2),
						 new Entity("Facebook Inc",null,null,null,null,null),
						 new Entity("Johnson & Johnson",12,12,12,12,12),
						 new Entity("General Electric",6,6,6,6,6),
						 new Entity("Oracle Corporation",null,null,null,null,null),
						 new Entity("Siemens AG",5,5,5,5,5),
						 new Entity("Pizza hut",null,null,null,null,null),
						 new Entity("The Walt Disney",21,21,21,21,21),
						 new Entity("Mastercard",null,null,null,null,null),
						 new Entity("Bank Of America",null,null,null,null,null)
						 )
						 ;
		}
} 


?>