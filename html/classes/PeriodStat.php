<?php 

	/**
	author: amassillo
	**/
	class PeriodStat { 
		private $name; 
		private $QoQ_change; 
		private $YoY_change; 
		private $open;
		private $closed;
		private $serieValues;
    

		function __construct($pName, $pQoQChange,$pYoYChange,$pOpen,$pClosed,$pSeriesValues) {
			$this->name = $pName;
			$this->QoQ_change = $pQoQChange;
			$this->YoY_change = $pYoYChange;
			$this->open 	  = $pOpen;
			$this->closed	  = $pClosed;
			$this->serieValues= $pSeriesValues;
		}
		function getName() { 
			return $this->name; 
		} 
		
		function getQoQChange() { 
			return $this->QoQ_change; 
		} 
		
		function getYoYChange() { 
			return $this->YoY_change; 
		} 
				
		function getOpen() { 
			return $this->open; 
		} 
		
		function getClosed() { 
			return $this->closed; 
		} 
	
		function getSerieValues(){
			return $this->serieValues;
		}
		
		function getStringSerieValues(){
			return implode(',',$this->serieValues);
		}
		static function getDashboardStats(){
			
			return array(new PeriodStat("Customs",10,-5,4,14,[4,5,7,6,8,8,8,9,10,20,20,18,22,17]),
						 new PeriodStat("E-Commerce",10,-5,3,12,[5,9,10,20,20,18,22,17,13,16,18,30]),
						 new PeriodStat("Track & Trace",10,-5,5,8,[4,5,7,6,8,16,19,34,17]),
						 new PeriodStat("Social Media",10,-5,13,3,[4,5,7,6,8,8,8,9,10,20,20,18,22,17]),
						 new PeriodStat("Test Buy",10,-5,6,2,[4,5,7,6,8,8,8,9,10,20,20,18,22,17]));
				
		}
} 


?>